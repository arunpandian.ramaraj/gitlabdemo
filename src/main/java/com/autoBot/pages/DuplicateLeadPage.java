package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class DuplicateLeadPage extends Annotations{

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") 
	WebElement elecreateLeadBu;

	public ViewLeadPage clickCreateLeadbutton() {
		click(elecreateLeadBu);
		return new ViewLeadPage();
	}
}
