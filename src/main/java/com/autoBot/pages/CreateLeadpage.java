package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadpage extends Annotations{

	public CreateLeadpage() {
		PageFactory.initElements(driver, this); 
	}
	
	@CacheLookup
	@FindBy(how=How.ID, using="createLeadForm_companyName") 
	WebElement eleEnterCN;
	@FindBy(how=How.ID, using="createLeadForm_firstName") 
	WebElement eleEnterFN;
	@FindBy(how=How.ID, using="createLeadForm_lastName") 
	WebElement eleEnterLN;
	@FindBy(how=How.ID, using="createLeadForm_primaryPhoneNumber") 
	WebElement eleEnterPN;
	@FindBy(how=How.ID, using="createLeadForm_primaryEmail") 
	WebElement eleEnterEI;
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") 
	WebElement elecreateLead;
	
	public CreateLeadpage enterCompanyName(String CN) {
		clearAndType(eleEnterCN, CN);
		return this;
	}
	public CreateLeadpage enterFirstName(String FN) {
		clearAndType(eleEnterFN, FN);
		return this;
	}
	public CreateLeadpage enterLastName(String LN) {
		clearAndType(eleEnterLN, LN);
		return this;
	}
	public CreateLeadpage enterPhoneNO(String PN) {
		clearAndType(eleEnterPN, PN);
		return this;
	}
	public CreateLeadpage enterEmailId(String Email) {
		clearAndType(eleEnterEI, Email);
		return this;
	}
	public ViewLeadPage clickCreateLeadbutton() {
		click(elecreateLead);
		return new ViewLeadPage();
	}
	
}
