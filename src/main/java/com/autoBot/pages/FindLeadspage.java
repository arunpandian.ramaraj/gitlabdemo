package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.autoBot.testng.api.base.Annotations;

public class FindLeadspage extends Annotations{

	public FindLeadspage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.XPATH, using="(//label[text()='First name:'])[3]/following::input[1]") 
	WebElement eleEnterFN;
	@FindBy(how=How.XPATH, using="(//label[text()='Last name:'])[3]/following::input[1]") 
	WebElement eleEnterLN;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") 
	WebElement eleFindLead;
	@FindBy(how=How.XPATH, using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]") 
	WebElement eleFirstDisplayedLeadId;
	@FindBy(how=How.XPATH, using="//span[text()='Phone']") 
	WebElement elePhonetab;
	@FindBy(how=How.XPATH, using="//span[text()='Email']") 
	WebElement eleEmailtab;
	@FindBy(how=How.NAME, using="phoneNumber") 
	WebElement elephoneNumber;
	@FindBy(how=How.NAME, using="emailAddress") 
	WebElement eleemailAddress;
	@FindBy(how=How.XPATH, using="(//label[text()='Lead ID:'])[1]/following::input[1]") 
	WebElement eleLeadID;
	@FindBy(how=How.XPATH, using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]") 
	WebElement eledeleteLeadID;
	

	public FindLeadspage enterFirstName(String FN) {
		clearAndType(eleEnterFN, FN);
		return this;
	}
	public FindLeadspage enterLastName(String LN) {
		clearAndType(eleEnterLN, LN);
		return this;
	}
	public FindLeadspage clickFindLeadsButton() {
		click(eleFindLead);
		return this;
	}

	public ViewLeadPage clickFirstDisplayedLeadId() throws InterruptedException {
		Thread.sleep(3000);
		clickWithNoSnap(eleFirstDisplayedLeadId);
		return new ViewLeadPage();
	}
	public FindLeadspage clickFirstDisplayedLeadIdMerge() throws InterruptedException {
		Thread.sleep(3000);
		clickWithNoSnap(eleFirstDisplayedLeadId);
		return this;
	}
	
	public FindLeadspage clickPhonetab() {
		click(elePhonetab);
		return this;
	}
	public FindLeadspage clickEmailtab() {
		click(eleEmailtab);
		return this;
	}
	public FindLeadspage enterPhoneNo(String PN) {
		clearAndType(elephoneNumber, PN);
		return this;
	}
	public FindLeadspage enterEmailId(String Email) {
		clearAndType(eleemailAddress, Email);
		return this;
	}
	public FindLeadspage enterLeadId() {
		clearAndType(eleLeadID, "11794");
		return this;
	}
	
	public FindLeadspage enterFromLeadId() {
		clearAndType(eleLeadID, "11794");
		return this;
	}
	public FindLeadspage enterToLeadId() {
		clearAndType(eleLeadID, "11795");
		return this;
	}
	public FindLeadspage VerifydeleteLead() throws InterruptedException {
		Thread.sleep(2000);
		verifyDisplayed(eledeleteLeadID);
		//	System.out.println(driver.findElementByXPath("//div[@class='x-paging-info']").getText());
		return this;
	}
	public FindLeadspage WindowHandleFindLeads() {
		switchToWindow(1);
		return new FindLeadspage();
	}
	public MergeLeadpage WindowHandleReturnFindLeads() {
		switchToWindow(0);
		return new MergeLeadpage();
	}

}
