package com.autoBot.pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class MergeLeadpage extends Annotations{

	public MergeLeadpage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[1]") 
	WebElement eleClickIconFromLead;
	@FindBy(how=How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[2]") 
	WebElement eleClickIconToLead;
	@FindBy(how=How.LINK_TEXT, using="Merge") 
	WebElement eleMergeLead;

	public FindLeadspage clickIconFromLead() {
		click(eleClickIconFromLead);
		return new FindLeadspage();
	}
	public FindLeadspage clickIconToLead() {
		click(eleClickIconToLead);
		return new FindLeadspage();
	}
	public MergeLeadpage clickMergeLead() {
		click(eleMergeLead);
		return this;
	}
	public MyLeadHomepage alerthandle() {
		acceptAlert();
		return new MyLeadHomepage();
	}
}
