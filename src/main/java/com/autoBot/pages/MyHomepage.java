package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class MyHomepage extends Annotations{

	public MyHomepage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how = How.LINK_TEXT, using="Leads")
	WebElement eleLEadTab;

	public MyLeadHomepage clickLeadTab() {
		click(eleLEadTab);
		return new MyLeadHomepage();
	}
	
}
