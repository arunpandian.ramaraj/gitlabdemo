package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.ID, using="viewLead_firstName_sp") 
	WebElement eleFirstname;
	@FindBy(how=How.XPATH, using="//a[@class='subMenuButton'][3]") 
	WebElement eleEditButton;
	@FindBy(how=How.XPATH, using="//a[@class='subMenuButtonDangerous']") 
	WebElement eleDeleteButton;
	@FindBy(how=How.XPATH, using="//a[text()='Duplicate Lead']']") 
	WebElement eleDupliButton;
	
	
	public HomePage verifyFirstName(String FN) {
		verifyExactText(eleFirstname, FN);
		return new HomePage();
	}
	
	public EditLeadPage clickEditButton() {
		click(eleEditButton);
		return new EditLeadPage();
	}
	public MyLeadHomepage clickDeleteButton() {
		click(eleDeleteButton);
		return new MyLeadHomepage();
	}
	public DuplicateLeadPage clickDuplicateLeadButton() {
		click(eleDupliButton);
		return new DuplicateLeadPage();
	}
	
	public HomePage verifyDuplicateLead(String FN, String LN) {
		String FirstNameafter = driver.findElementById("viewLead_firstName_sp").getText();
		String LastNameafter = driver.findElementById("viewLead_lastName_sp").getText();
		if(FirstNameafter.equals(FN)&&LastNameafter.equals(LN))
		{
			System.out.println("Dupicated Lead Created");
		}
		else
		{
			System.out.println("Not a Dupicated Lead");
		}
		return new HomePage();
	}
	
	public HomePage verifyUpdatedCN() {
		String companyname = driver.findElementById("viewLead_companyName_sp").getText(); 
		String afterrplace = companyname.replaceAll("\\d", "").replaceAll("\\W","");
		if(afterrplace.equals("Indium")) 
		{
			System.out.println("Company name updated"); 
		} 
		else 
		{
			System.out.println("Company name not updated"); 
		} 
		return new HomePage();

	}
		
}
