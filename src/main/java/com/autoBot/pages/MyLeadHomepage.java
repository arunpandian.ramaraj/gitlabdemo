package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadHomepage extends Annotations{

	public MyLeadHomepage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how = How.XPATH,using ="//a[text()='Create Lead']")
	WebElement eleClickCreateLead;
	@FindBy(how = How.LINK_TEXT,using ="Find Leads")
	WebElement eleClickfindLeads;
	@FindBy(how = How.LINK_TEXT,using ="Merge Leads")
	WebElement eleClickMergeLeads;
	
	public CreateLeadpage clickCreateLead() {
		click(eleClickCreateLead);
		return new CreateLeadpage();
	}
	
	public FindLeadspage clickfindLeads() {
		click(eleClickfindLeads);
		return new FindLeadspage();
	}
	public MergeLeadpage clickMergeLeads() {
		click(eleClickMergeLeads);
		return new MergeLeadpage();
	}
}
