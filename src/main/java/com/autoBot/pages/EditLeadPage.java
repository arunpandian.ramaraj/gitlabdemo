package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class EditLeadPage extends Annotations{

	public EditLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.ID, using="updateLeadForm_companyName") 
	WebElement eleEnterUpCN;
	@FindBy(how=How.XPATH, using="//input[@value='Update']") 
	WebElement eleclickUpdate;

	public EditLeadPage enterCompanyName() {
		clearAndType(eleEnterUpCN, "Indium");
		return this;
	}
	
	public ViewLeadPage clickUpdatebutton() {
		click(eleclickUpdate);
		return new ViewLeadPage();
	}
	
}
