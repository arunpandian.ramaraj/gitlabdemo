package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(how=How.TAG_NAME, using="h2") 
	WebElement eleloginverify;
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") 
	WebElement crmsfaLink;
	
	
	public HomePage verifyLoginName() {
		verifyExactText(eleloginverify, "Demo");
		return this;
	}
	
	public MyHomepage clickCRMSFA() {
		click(crmsfaLink);
		return new MyHomepage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
