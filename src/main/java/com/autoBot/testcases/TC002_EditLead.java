package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_EditLead extends Annotations {
	
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_EditLead";
		testcaseDec = "Login into leaftaps";
		author = "Arun";
		category = "smoke";
		excelFileName = "T001_3";
	}
	
	@Test(dataProvider = "FetchData")
	public void runEdit(String Username, String Password, String CompanyName, String FirstName, String LastName, String PhoneNumber, String EmailId) throws InterruptedException {

		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.clickCRMSFA()
		.clickLeadTab()
		.clickfindLeads()
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.clickFindLeadsButton()
		.clickFirstDisplayedLeadId()
		.clickEditButton()
		.enterCompanyName()
		.clickUpdatebutton()
		.verifyUpdatedCN();
		
	}

}
