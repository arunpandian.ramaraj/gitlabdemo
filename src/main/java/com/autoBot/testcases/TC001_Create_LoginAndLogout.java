package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;
// Annotations == ProjectBase
public class TC001_Create_LoginAndLogout extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_Create_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "Arun";
		category = "smoke";
		excelFileName = "T001_3";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String Username, String Password, String CompanyName, String FirstName, String LastName, String PhoneNumber, String EmailId) {

		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.verifyLoginName()
		.clickCRMSFA()
		.clickLeadTab()
		.clickCreateLead()
		.enterCompanyName(CompanyName)
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.enterPhoneNO(PhoneNumber)
		.enterEmailId(EmailId)
		.clickCreateLeadbutton()
		.verifyFirstName(FirstName);
	}
}
	





